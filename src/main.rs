
use std::net::SocketAddr;


use figment::{Figment, providers::{Format, Toml}};
use serde::Deserialize;

use actix_web::{get, web, App, HttpServer, Responder, HttpResponse};


#[derive(Debug, PartialEq, Deserialize)]
struct AppConfig {
    port: u16,
    wordpress_url: String,
}


#[get("{path:.*}")]
async fn greet(data: web::Data<String>, path: web::Path<String>) -> impl Responder {
    let wordpress_url: &str = &data;
    let total_url = format!("{wordpress_url}{path}");
    println!("{total_url}");

    let resp = reqwest::get(&total_url).await.expect("Getting error");
    let headers = resp.headers();
    let content_type = match headers.get("content-type") {
        Some(t) => t.to_str().unwrap_or("text/plain"),
        None => "text/plain"
    };

    let mut final_res = HttpResponse::build(resp.status());
    
    println!("{content_type}");

    if content_type.starts_with("text/html" ){
        let body_text = resp.text().await.expect("cannot convery to text");

        // minimize load time
        // instead of removing html here
        // insert js to remove html at client side

        let body_text_no_marketing = body_text.replace("marketing-bar", "").replace("Blog at WordPress.com.", "");

        let remove_banner_js = "<script>console.log(1);document.querySelector('#marketingbar').remove();document.body.classList.remove('has-marketing-bar');</script>";
        let remove_action_js = "<script>document.querySelector('#actionbar').remove();</script>";
        let replace_links_js = format!("<script>document.querySelectorAll('a').forEach(a => a.href=a.href.replace('{}', '/'))</script>", wordpress_url);
        let remove_blog_tail_js = "<script>['.wp-block-comments','#jp-post-flair'].forEach(i => document.querySelector(i).remove())</script>";


        let revised_body_text = format!("{body_text_no_marketing}{remove_banner_js}{remove_action_js}{replace_links_js}{remove_blog_tail_js}");


        final_res.body(revised_body_text)
    } else {
        final_res.body(resp.bytes().await.expect("convert to bytes err"))
    }
}

#[actix_web::main] // or #[tokio::main]
async fn main() -> std::io::Result<()> {
    let figment = Figment::from(Toml::file("config.toml"));
    let config: AppConfig = figment.extract().expect("Unable to parse config");


    let addr = SocketAddr::from(([127, 0, 0, 1], config.port));
    let data = web::Data::new(config.wordpress_url);

    HttpServer::new(move || {
        App::new().app_data(data.clone()).service(greet)
    })
    .bind(addr)?
    .run()
    .await

}